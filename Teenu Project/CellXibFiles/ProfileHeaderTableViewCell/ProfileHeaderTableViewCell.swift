//
//  ProfileHeaderTableViewCell.swift
//  Teenu Project
//
//  Created by Teenu Puthenthoppil on 21/03/2020.
//  Copyright © 2020 Teenu Puthenthoppil. All rights reserved.
//

import UIKit

class ProfileHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(_ model:Friend) {
        profileImage.loadFromUrl(model.imageURL ?? "")
        let date = model.dateOfBirth?.getDayMonYearFromString()
        let str = "\(model.firstName ?? "") \(model.lastName ?? "")\n\(date ?? "")"
        let attributedString = NSMutableAttributedString(string: str, attributes: [
            .font: UIFont.systemFont(ofSize: 25),
            .foregroundColor: UIColor.black
        ])
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 15, weight: .light), range: NSRange(location: "\(model.firstName ?? "") \(model.lastName ?? "")".count, length: "\n\(date ?? "")".count))
        
        contentLabel.attributedText = attributedString
    }
}
