//
//  FullWidthButtonsTableViewCell.swift
//  Teenu Project
//
//  Created by Teenu Puthenthoppil on 21/03/2020.
//  Copyright © 2020 Teenu Puthenthoppil. All rights reserved.
//

import UIKit

class FullWidthButtonsTableViewCell: UITableViewCell {

    @IBOutlet var btnOutlet: UIButton!
    var actionComplete: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnAction(_ sender: UIButton) {
        self.actionComplete?()
    }
}
