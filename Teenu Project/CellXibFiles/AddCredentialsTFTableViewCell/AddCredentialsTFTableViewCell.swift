//
//  AddCredentialsTFTableViewCell.swift
//  GarGish
//
//  Created by Teenu Puthenthoppil on 30/12/2019.
//  Copyright © 2019 Teenu Puthenthoppil. All rights reserved.
//

import UIKit

class AddCredentialsTFTableViewCell: UITableViewCell {
    
    @IBOutlet weak var enterCreadTF: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
   
    func setContent(_ row:Int, _ data:SignDataModel) {
        enterCreadTF.tag = row
        switch row {
        case 0:
            enterCreadTF.text = data.email
            enterCreadTF.placeholder = "Username"
        default:
            enterCreadTF.text = data.password
            enterCreadTF.placeholder = "Password"
            enterCreadTF.isSecureTextEntry = true
        }
    }
    
}
