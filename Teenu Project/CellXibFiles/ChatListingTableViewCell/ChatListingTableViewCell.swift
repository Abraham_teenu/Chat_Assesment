//
//  ChatListingTableViewCell.swift
//  GarGish
//
//  Created by Teenu Puthenthoppil on 31/12/2019.
//  Copyright © 2019 Teenu Puthenthoppil. All rights reserved.
//

import UIKit

class ChatListingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var chatHeadImage: UIImageView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var pendingToSeeLbl: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userLastMessageIndexLbl: UILabel!
    @IBOutlet weak var newMessageIndicator: UIView!
    @IBOutlet var infoBtn: UIButton!
    
    var actionComplete: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func infoBtnAction(_ sender: UIButton) {
       actionComplete?()
    }
    
    func setData(_ model:Friend, _ tag:Int) {
        infoBtn.tag = tag
        
        chatHeadImage.loadFromUrl(model.imageURL ?? "")
        
        // uncomment to remove sdwebimage
        //chatHeadImage.load(url: (model.imageURL ?? "").toUrl())
        
        userName.text = model.alias ?? ""
        switch model.status {
        case "Busy":
            newMessageIndicator.backgroundColor = .yellow
        case "Online":
            newMessageIndicator.backgroundColor = .green
        default:
            newMessageIndicator.backgroundColor = .red
        }
    }
}
