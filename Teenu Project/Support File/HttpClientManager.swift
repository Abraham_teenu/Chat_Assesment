import UIKit
import Alamofire
import ARSLineProgress

protocol AB_CommonCodable: Codable {
    var result: Bool { get set }
    var error: String? { get set }
}

enum SHOWHIDEHUD {
    case SHOW
    case HIDE
}

class HttpClientManager: NSObject {
    
    //MARK: - Shared Instance
    static let SharedHM = HttpClientManager()
    
    func showhideHUD(viewtype: SHOWHIDEHUD,showType: showType) {
        switch viewtype {
        case .SHOW:
            DispatchQueue.main.async {
                ProgressHUD.show(showType)
            }
        case .HIDE:
            DispatchQueue.main.async {
                ProgressHUD.hide()
            }
        }
    }
}

extension HttpClientManager {
    
    func serverRequest<T>(from url: String,
                          params: [String:Any]?,
                          method: HTTPMethod,
                          decodingType: T.Type,
                          needLoader: Bool = true,
                          loaderEnd: showType = .normal,
                          completionHandler completion: @escaping (
        _ success: Bool,
        _ object: T?,
        _ Alert: String?) -> ())
        -> Void where T: AB_CommonCodable {
            
            let headers: HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
            if needLoader {
                showhideHUD(viewtype: .SHOW, showType: .normal)
            }
            
            AF.request(url, method: method, parameters: params
                , encoding: JSONEncoding.default ,
                  headers: headers).responseString {
                    response in
                    print(response)
                    if let jsonData = response.data {
                        
                        let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
                        let str = jsonString?.data(using: .utf8)!.prettyPrintedJSONString
                        print(str ?? "")
                        
                        do {
                            let Object = try JSONDecoder().decode(decodingType, from: jsonData)
                            if needLoader {
                                if loaderEnd == .normal {
                                    self.showhideHUD(viewtype: .HIDE, showType: .normal)
                                } else {
                                    self.showhideHUD(viewtype: .SHOW, showType: loaderEnd)
                                }
                            }
                            completion(Object.result, Object, Object.error)
                        }
                        catch {
                            if needLoader {
                                self.showhideHUD(viewtype: .SHOW, showType: .fail)
                            }
                            completion(false,nil, error.localizedDescription)
                            print("parsing err:", error)
                        }
                    }
                    else
                    {
                        if needLoader {
                            self.showhideHUD(viewtype: .SHOW, showType: .fail)
                        }
                        completion(false, nil, "Something went wrong.")
                    }
            }
    }
}

extension Data {
    var prettyPrintedJSONString: NSString? {
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
            let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
            let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }
        
        return prettyPrintedString
    }
}
