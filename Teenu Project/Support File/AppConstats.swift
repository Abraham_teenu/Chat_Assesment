
import UIKit
struct AppDetails {
    
    static let APP_NAME = "App Name"
    static let ITUNES_URL = ""
    static let BUNDIL_ID = ""
    
}
let appsession = UIApplication.shared.delegate as! AppDelegate
var sbMain = UIStoryboard(name: "Main", bundle: nil)

struct AppURL {
    
    static let BASE_URL = "http://mobileexam.dstv.com/"
    static let IMAGE_BASE_URL = "http://mobileexam.dstv.com/login"
    
    static let SIGN_IN_API = BASE_URL + "login"
    static let FRIENDS_API = BASE_URL + "friends"
    
    
}

struct AppAlertMsg {
    //App Alert Msg
    static let NetWorkAlertMessage = "no_internet"
    static let serverNotReached = "The server could not be reached because of a connection problem"
    static let NoNotification = "No notification"
    static let NoUsername = "Please enter username"
    static let NoPassword = "Please enter password"
    
   
}

struct KeyConstants {
    static let guid = "guid"
    static let firstName = "firstName"
    static let signedIn = "sign_success"
}

struct AppButtonTitles {
    static let okTitle = "OK"
    static let cancel = "Cancel"
    static let retry = "Retry"
   
}

struct storyboardIdentifier {
    
    static let mainStoryBoard   = "Main"
    
}

struct CellIdentifier {
    //App cell Identifiers
    
    static let FullWidthButtonsTableViewCell = "FullWidthButtonsTableViewCell"
    static let AddCredentialsTFTableViewCell = "AddCredentialsTFTableViewCell"
    static let ChatListingTableViewCell = "ChatListingTableViewCell"
    static let ProfileHeaderTableViewCell = "ProfileHeaderTableViewCell"
    
}

struct Default_Key {
    static var LAUNCHED_BEFORE = "launchedBefore"
   
}
