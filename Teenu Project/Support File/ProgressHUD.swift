//
//  ProgressHUD.swift
//
//  Created by Teenu Puthenthoppil on 24/05/17.
//  Copyright © 2020 Teenu puthenthoppil. All rights reserved.
//

import UIKit
import ARSLineProgress

enum showType {
    case normal
    case success
    case fail
    case nothing
}

class ProgressHUD: UIView {
    static let sharedInstance = Bundle.main.loadNibNamed("ProgressHUD", owner: nil)?[0] as! ProgressHUD
    
    override init(frame:CGRect) {
        super.init(frame: frame)
    }
    override func updateConstraints() {
        self.frame = (self.window?.bounds)!
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    class func show(_ progresstype:showType) {
        let hud: ProgressHUD? = self.sharedInstance
        let mainWindow: UIWindow? = UIApplication.shared.windows.first
        mainWindow?.addSubview(hud!)
        switch progresstype {
        case .success:
            DispatchQueue.main.async {
                let hud: ProgressHUD? = self.sharedInstance
                if ((hud?.superview) != nil) {
                    hud?.removeFromSuperview()
                }
                ARSLineProgress.showSuccess()
            }
        case .fail:
            DispatchQueue.main.async {
                let hud: ProgressHUD? = self.sharedInstance
                if ((hud?.superview) != nil) {
                    hud?.removeFromSuperview()
                }
                ARSLineProgress.showFail()
            }
        case .normal:
            DispatchQueue.main.async {
                ARSLineProgress.show()
            }
        default:
            break
        }
    }
    
    class func hide() {
        DispatchQueue.main.async {
            let hud: ProgressHUD? = self.sharedInstance
            if ((hud?.superview) != nil) {
                hud?.removeFromSuperview()
            }
            ARSLineProgress.hide()
        }
    }
}
