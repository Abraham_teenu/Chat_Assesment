//
//  UserListViewModel.swift
//  Teenu Project
//
//  Created by Teenu Puthenthoppil on 19/03/2020.
//  Copyright © 2020 Teenu Puthenthoppil. All rights reserved.
//

import Foundation
import UIKit

struct ChatListModel: AB_CommonCodable {
    var result: Bool
    var error: String?
    let friends: [Friend]?
    
    enum CodingKeys: String, CodingKey {
        case error
        case result
        case friends
    }
}

// MARK: - Friend
struct Friend: Codable {
    let firstName, lastName, alias, dateOfBirth: String?
    let imageURL: String?
    let status, lastSeen: String?
}




class UserListViewModel: NSObject {
    var cellHeights: [IndexPath : CGFloat] = [:]
    var actionComplete: ((_ index:Int) -> Void)?
    var friendsData : [Friend]?
    
    
    func getChatList(success: @escaping ((_ alert: String)->()),failure: @escaping (_ alert: String)->()) {
        
        let appendID = ";uniqueID=\(KeychainWrapper.standard.string(forKey: KeyConstants.guid) ?? "");name=\(KeychainWrapper.standard.string(forKey: KeyConstants.firstName) ?? "")"
        
        HttpClientManager.SharedHM.serverRequest(from: AppURL.FRIENDS_API + appendID, params: nil, method: .get
        , decodingType: ChatListModel.self, needLoader: true) { (status ,object, alert) in
            if status {
                self.friendsData = object?.friends
                success("")
            } else {
                failure(alert ?? "Something went wrong")
            }
            
        }
    }
    
}

extension UserListViewModel : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.cellHeights[indexPath] ?? UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 12
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendsData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.ChatListingTableViewCell, for: indexPath) as? ChatListingTableViewCell {
            cell.clipsToBounds = true
            cell.layoutSubviews()
            cell.layoutIfNeeded()
            
            cell.actionComplete = { [weak self] in
                self?.actionComplete?(cell.infoBtn.tag)
            }
    
            if let data = friendsData?[indexPath.row] {
                cell.setData(data, indexPath.row)
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    
}
