//
//  UserListViewController.swift
//  Teenu Project
//
//  Created by Teenu Puthenthoppil on 19/03/2020.
//  Copyright © 2020 Teenu Puthenthoppil. All rights reserved.
//

import UIKit

class UserListViewController: CommonViewController {
    
    let viewModel = UserListViewModel()
    
    lazy var pageStatusLbl: UILabel = UILabel.pageStatusLbl()
    let userListTV = UITableView(frame: .zero, style: .grouped)
    var safeArea: UILayoutGuide!
    
    override func loadView() {
        super.loadView()
        
        safeArea = view.layoutMarginsGuide
        setupTableView()
    }
    
    func setupTableView() {
        
        view.addSubview(userListTV)
        view.addSubview(pageStatusLbl)
        view.backgroundColor = .white
        
        pageStatusLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pageStatusLbl.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        pageStatusLbl.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        pageStatusLbl.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        
        userListTV.backgroundColor = UIColor.white
        userListTV.separatorStyle = .none
        userListTV.translatesAutoresizingMaskIntoConstraints = false
        userListTV.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        userListTV.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        userListTV.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        userListTV.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        userListTV.dataSource = self.viewModel
        userListTV.delegate = self.viewModel
        
        userListTV.register(UINib(nibName: CellIdentifier.ChatListingTableViewCell, bundle:nil), forCellReuseIdentifier: CellIdentifier.ChatListingTableViewCell)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setNavigationBarWithBack(success: {}, name: "Friends", rightSideImage: "", backNeeded: "false")
        getUserListApi()
        setCallBacks()
    }
    
    func getUserListApi() {
        
        guard self.noInternetAlert(handler: {
            self.getUserListApi()
        }) else {
            return
        }
        self.viewModel.getChatList(success: { alert in
            self.userListTV.reloadData()
        }, failure: {
            alert in
            self.showToast(message: alert)
        })
    }
    
    func setCallBacks() {
        
        self.viewModel.actionComplete = { [unowned self] row in
            if let data = self.viewModel.friendsData?[row] {
                self.presentTo(name: "", controller: UserInfoViewController(), with: ["data":data], modelPresentStyle: .fullScreen, animated: false, type: CATransitionType.fade , subtype: CATransitionSubtype.fromTop)
            }
        }
    }
}
