//
//  CommonViewController.swift
//  Teenu Project
//
//  Created by Teenu Puthenthoppil on 19/03/2020.
//  Copyright © 2020 Teenu Puthenthoppil. All rights reserved.
//

import UIKit

class CommonViewController: UIViewController, UIGestureRecognizerDelegate {

    var param: [String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
