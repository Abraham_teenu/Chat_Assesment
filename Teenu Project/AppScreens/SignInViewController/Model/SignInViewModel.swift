//
//  SignInViewModel.swift
//  GarGish
//
//  Created by Teenu Puthenthoppil on 30/12/2019.
//  Copyright © 2019 Teenu Puthenthoppil. All rights reserved.
//

import Foundation
import UIKit

extension CGRect {
    
    init(_ x:CGFloat, _ y:CGFloat, _ w:CGFloat, _ h:CGFloat) {
        
        self.init(x:x, y:y, width:w, height:h)
    }
}

class SignDataModel : NSObject {
    var email : String? = ""
    var password : String? = ""
}

struct SignInModel: AB_CommonCodable {
    var result: Bool
    var error: String?
    let guid, firstName, lastName : String?
    
    enum CodingKeys: String, CodingKey {
        case error
        case result
        case guid
        case firstName
        case lastName
    }
}

class SignInViewModel  : NSObject {
    
    var cellHeights: [IndexPath : CGFloat] = [:]
    var actionComplete: (() -> Void)?
    var myClass = SignDataModel()
    
    
    func signInApi(success: @escaping ((_ alert: String)->()),failure: @escaping (_ alert: String)->()) {
        
        let param : [String:Any] = [
            "username":  myClass.email ?? "",
            "password": myClass.password ?? ""
        ]
        
        HttpClientManager.SharedHM.serverRequest(from: AppURL.SIGN_IN_API, params: param, method: .post
        , decodingType: SignInModel.self, needLoader: true) { (status ,object, alert) in
            if status {
                KeychainWrapper.standard.set(object?.guid ?? "", forKey: KeyConstants.guid)
                KeychainWrapper.standard.set(object?.firstName ?? "", forKey: KeyConstants.firstName)
                UserDefaults.standard.set(true, forKey: KeyConstants.signedIn)
                success("")
            } else {
                failure(alert ?? "Something went wrong")
            }
            
        }
    }
    
}

extension SignInViewModel : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.cellHeights[indexPath] ?? UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 12
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0, 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.AddCredentialsTFTableViewCell, for: indexPath) as? AddCredentialsTFTableViewCell {
                cell.clipsToBounds = true
                cell.layoutSubviews()
                cell.layoutIfNeeded()
                cell.setContent(indexPath.row, myClass)
                cell.enterCreadTF.addTarget(self, action: #selector(self.textFieldTarget), for: .editingChanged)
                return cell
            }
        default:
            if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.FullWidthButtonsTableViewCell, for: indexPath) as? FullWidthButtonsTableViewCell {
                cell.clipsToBounds = true
                cell.layoutSubviews()
                cell.layoutIfNeeded()
                cell.actionComplete = { [unowned self] in
                    self.actionComplete?()
                }
                cell.btnOutlet.setTitle("Log In", for: .normal)
                
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
    @objc func textFieldTarget(textField: UITextField) {
        switch textField.tag {
        case 0:
            myClass.email = textField.text
        default:
            myClass.password = textField.text
        }
    }
    
    
}
