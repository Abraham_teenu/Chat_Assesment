//
//  SignInViewController.swift
//  Teenu Project
//
//  Created by Teenu Puthenthoppil on 19/03/2020.
//  Copyright © 2020 Teenu Puthenthoppil. All rights reserved.
//

import UIKit
extension UILabel {
    
    static func pageStatusLbl() -> UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }
}

class SignInViewController: CommonViewController {
    
    let viewModel = SignInViewModel()
    
    lazy var pageStatusLbl: UILabel = UILabel.pageStatusLbl()
    let signInTV = UITableView(frame: .zero, style: .grouped)
    var safeArea: UILayoutGuide!
    
    override func loadView() {
        super.loadView()
        
        safeArea = view.layoutMarginsGuide
        setupTableView()
    }
    
    func setupTableView() {
        
        view.addSubview(signInTV)
        view.addSubview(pageStatusLbl)
        view.backgroundColor = .white
        
        pageStatusLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pageStatusLbl.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        pageStatusLbl.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        pageStatusLbl.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        
        signInTV.backgroundColor = UIColor.clear
        signInTV.separatorStyle = .none
        signInTV.translatesAutoresizingMaskIntoConstraints = false
        signInTV.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        signInTV.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        signInTV.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        signInTV.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        signInTV.dataSource = self.viewModel
        signInTV.delegate = self.viewModel
        
        
        self.signInTV.register(UINib(nibName: CellIdentifier.AddCredentialsTFTableViewCell, bundle:nil), forCellReuseIdentifier: CellIdentifier.AddCredentialsTFTableViewCell)
        
        self.signInTV.register(UINib(nibName: CellIdentifier.FullWidthButtonsTableViewCell, bundle:nil), forCellReuseIdentifier: CellIdentifier.FullWidthButtonsTableViewCell)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setCallBacks()
        setNavigationBarWithBack(success: {}, name: "LOG IN", rightSideImage: "", backNeeded: "false")
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setCallBacks(){
        self.viewModel.actionComplete = { [unowned self] in
            
            self.verifyFields()
            
        }
    }
    
    
    func verifyFields() {
        
        guard let email = self.viewModel.myClass.email,(email.trimmingCharacters(in: .whitespaces).count > 0) else {
            return showToast(message: AppAlertMsg.NoUsername)
        }
        
        guard let password = self.viewModel.myClass.password, (password.trimmingCharacters(in: .whitespaces).count > 0) else {
            return showToast(message: AppAlertMsg.NoPassword)
        }
        
        callSignInApi()
        
    }
    
    func callSignInApi() {
        
        guard self.noInternetAlert(handler: {
            self.callSignInApi()
        }) else {
            return
        }
        self.viewModel.signInApi(success: { alert in
            UserListViewController().setRootViewController()
        }, failure: {
            alert in
            self.askConfirmationRandomButtons(title: AppDetails.APP_NAME, message: alert, btn_title: [AppButtonTitles.okTitle], completion: { _ in
                
            })
        })
    }
}
