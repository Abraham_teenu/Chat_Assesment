//
//  ViewController.swift
//  Teenu Project
//
//  Created by Teenu Puthenthoppil on 19/03/2020.
//  Copyright © 2020 Teenu Puthenthoppil. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if UserDefaults.standard.bool(forKey: KeyConstants.signedIn) {
            DispatchQueue.main.async {
                UserListViewController().setRootViewController()
            }
        } else {
            DispatchQueue.main.async {
                SignInViewController().setRootViewController()
            }
        }
    }
    
    
}

