//
//  UserInfoViewController.swift
//  Teenu Project
//
//  Created by Teenu Puthenthoppil on 21/03/2020.
//  Copyright © 2020 Teenu Puthenthoppil. All rights reserved.
//

import UIKit

class UserInfoViewController: CommonViewController {

    let viewModel = UserInfoViewModel()
    
    lazy var pageStatusLbl: UILabel = UILabel.pageStatusLbl()
    let userInfoTV = UITableView(frame: .zero, style: .grouped)
    var safeArea: UILayoutGuide!
    
    override func loadView() {
        super.loadView()
        
        safeArea = view.layoutMarginsGuide
        setupTableView()
    }
    
    func setupTableView() {
        
        view.addSubview(userInfoTV)
        view.addSubview(pageStatusLbl)
        view.backgroundColor = .white
        
        pageStatusLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pageStatusLbl.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        pageStatusLbl.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        pageStatusLbl.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        
        userInfoTV.backgroundColor = UIColor.white
        userInfoTV.separatorStyle = .none
        userInfoTV.translatesAutoresizingMaskIntoConstraints = false
        userInfoTV.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        userInfoTV.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        userInfoTV.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        userInfoTV.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        userInfoTV.dataSource = self.viewModel
        userInfoTV.delegate = self.viewModel
        
        userInfoTV.register(UINib(nibName: CellIdentifier.ProfileHeaderTableViewCell, bundle:nil), forCellReuseIdentifier: CellIdentifier.ProfileHeaderTableViewCell)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavigationBarWithBack(success: {}, name: "Friends", rightSideImage: "")
        
        if let param = param {
            self.viewModel.model = param["data"] as? Friend
            self.userInfoTV.reloadData()
        }
    }
}
