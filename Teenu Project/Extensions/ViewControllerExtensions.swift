//
//  ViewControllerExtensions.swift
//  Teenu Project
//
//  Created by Teenu Puthenthoppil on 19/03/2020.
//  Copyright © 2020 Teenu Puthenthoppil. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

extension UIViewController {
    
    typealias NoInternetAlertHandler = ()-> Void
    
    func connectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        var flags : SCNetworkReachabilityFlags = []
        if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == false {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        if (isReachable && !needsConnection)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func noInternetAlert(handler: NoInternetAlertHandler?) -> Bool {
        if !connectedToNetwork() {
            let alertVC = UIAlertController(title: AppAlertMsg.NetWorkAlertMessage, message: nil, preferredStyle: .alert)
            let retryAction = UIAlertAction(title: AppButtonTitles.retry, style: .default, handler: { (alertAction) in
                if let handler = handler {
                    handler()
                }
                else {
                    self.showToast(message: AppAlertMsg.NetWorkAlertMessage)
                }
            })
            alertVC.addAction(retryAction)
            self.present(alertVC, animated: true)
            return false
        }
        else {
            return true
        }
    }
    
    func askConfirmationRandomButtons(title:String, message:String, btn_title : [String], completion:@escaping (_ result: Int) -> Void) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for i in 0...btn_title.count - 1 {
            if btn_title.count > 2 && i == btn_title.count - 1 {
                alert.addAction(UIAlertAction(title: btn_title[i], style: .destructive, handler: { (_) in
                    print("You've pressed the destructive")
                    completion(i)
                }))
            } else {
                alert.addAction(UIAlertAction(title: btn_title[i], style: .default, handler: { (_) in
                    print("You've pressed default")
                    completion(i)
                }))
            }
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func setRootViewController() {
        let vc = self
        let navigationController = UINavigationController(rootViewController: vc)
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
    }
    
    func pushTo(name: String, controller: CommonViewController? = nil, with param: [String: Any]) {
        let vc = controller
        vc?.param = param
        vc?.isModalInPresentation = false
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func presentTo(name: String, controller: CommonViewController? = nil, with param: [String: Any], modelPresentStyle: UIModalPresentationStyle? = .formSheet, animated: Bool? = true, type: CATransitionType? = .none, subtype: CATransitionSubtype? = .none) {
        let vc = controller
        vc?.param = param
        let navController = UINavigationController(rootViewController: vc!)
        if #available(iOS 13.0, *) {
            navController.isModalInPresentation = false
        }
        navController.modalPresentationStyle = modelPresentStyle!
        
        if !animated! {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = type!
        transition.subtype = subtype!
        self.view.window!.layer.add(transition, forKey: kCATransition)
        }
        
        self.present(navController, animated:animated!, completion: nil)
    }
    
    func setNavigationBarWithBack(success: @escaping (()->()), name:String, imageName:String = "backIcon", rightSideImage:String, backNeeded:String = "yes") {
        //backImage
        
        let leftBtnContainerView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        let leftBtn = UIButton(frame: leftBtnContainerView.frame)
        let leftImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        leftImageView.image = UIImage(named:imageName)
        leftImageView.center = leftBtnContainerView.center
        leftBtnContainerView.addSubview(leftImageView)
        leftBtnContainerView.addSubview(leftBtn)
        
        if rightSideImage.count > 0 {
            let rightSideView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            let rightSideBtn = UIButton(frame: rightSideView.frame)
            let rightSideimageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            rightSideimageView.image = UIImage(named:rightSideImage)
            rightSideimageView.center = leftBtnContainerView.center
            rightSideView.addSubview(rightSideimageView)
            rightSideView.addSubview(rightSideBtn)
            
            rightSideBtn.actionHandle(controlEvents: UIControl.Event.touchUpInside,
                                      ForAction:{() -> Void in
                                        success()
            })
            
            let rightSideBarBtnControl = UIBarButtonItem(customView: rightSideView)
            self.navigationItem.rightBarButtonItems = [rightSideBarBtnControl]
        }
        
        if backNeeded != "yes" {
            leftBtnContainerView.isHidden = true
            leftBtn.isHidden = true
            leftImageView.isHidden = true
        } else {
            leftBtn.addTarget(self, action: #selector(returnBack), for: UIControl.Event.touchUpInside)
            let back = UIBarButtonItem(customView: leftBtnContainerView)
            
            self.navigationItem.leftBarButtonItems = [back]
        }
        self.navigationItem.title = name
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        UINavigationBar.appearance().backgroundColor = UIColor.white
    }
    
    @objc func returnBack()  {
        
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
            let vc = nav.viewControllers.first
            if vc == self {
                nav.dismiss(animated: true) {
                }
            }
        } else {
            dismiss()
        }
    }
    
    @objc func dismiss()  {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension UIButton {
    private func actionHandleBlock(action:(() -> Void)? = nil) {
        struct __ {
            static var action :(() -> Void)?
        }
        if action != nil {
            __.action = action
        } else {
            __.action?()
        }
    }
    
    @objc private func triggerActionHandleBlock() {
        self.actionHandleBlock()
    }
    
    func actionHandle(controlEvents control :UIControl.Event, ForAction action:@escaping () -> Void) {
        self.actionHandleBlock(action: action)
        self.addTarget(self, action: Selector(("triggerActionHandleBlock")), for: control)
    }
}

extension UIViewController {
    func showToast(message: String, completion: @escaping (_ success: Bool) -> Void = {_ in }) {
        
        guard message.trimmingCharacters(in: .whitespaces).count > 0 else {return}
        guard let window = UIApplication.shared.keyWindow else {return}
        let messageLbl = UILabel()
        messageLbl.text = message
        messageLbl.textAlignment = .center
        messageLbl.font = UIFont(name: "Avenir-Book", size: 15.0)!
        messageLbl.textColor = .white
        messageLbl.backgroundColor = UIColor(white: 0, alpha: 0.9)
        messageLbl.numberOfLines = 0
        messageLbl.lineBreakMode = .byWordWrapping
        messageLbl.sizeToFit()
        
        let textSize:CGSize = messageLbl.intrinsicContentSize
        let labelWidth = min(textSize.width, window.frame.width - 40)
        let height = heightForView(text: message, font: UIFont(name: "Avenir-Book", size: 15.0)!, width: labelWidth + 30)
        messageLbl.frame = CGRect(x: 20, y: 64, width: labelWidth + 30, height: height + 20)
        messageLbl.center.x = window.center.x
        messageLbl.layer.cornerRadius = 5
        messageLbl.layer.masksToBounds = true
        window.addSubview(messageLbl)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            UIView.animate(withDuration: 2, animations: {
                messageLbl.alpha = 0
            }) { (_) in
                messageLbl.removeFromSuperview()
                completion(true)
            }
        }
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    func isValidEmail(testStr:String) -> Bool {
        guard testStr.trimmingCharacters(in: .whitespaces).count < 254 else {
            return false
        }
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
