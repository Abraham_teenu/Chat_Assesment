//
//  StringExtensions.swift
//  Teenu Project
//
//  Created by Teenu Puthenthoppil on 22/03/2020.
//  Copyright © 2020 Teenu Puthenthoppil. All rights reserved.
//

import Foundation

extension String {
    func getDayMonYearFromString(format:String = "yyyy-MM-dd") -> String {
        if self != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = format
            let date = dateFormatter.date(from: self)
            dateFormatter.dateFormat = "dd MMM yyyy"
            dateFormatter.locale = Locale(identifier: "en_EN")
            let dateInStrFormat = dateFormatter.string(from: date!)
            return dateInStrFormat
        } else {
            return self
        }
    }
    
    func toUrl() -> URL {
        let url = URL(string: self)
        return url!
    }
}
