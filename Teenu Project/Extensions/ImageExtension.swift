//
//  ImageExtension.swift
//  Teenu Project
//
//  Created by Teenu Puthenthoppil on 19/03/2020.
//  Copyright © 2020 Teenu Puthenthoppil. All rights reserved.
//

import Foundation
import SDWebImage

extension UIImageView {
    
    func loadFromUrl(_ url:String) {
        self.sd_setImage(with: URL(string: url) , placeholderImage: nil, options: [.scaleDownLargeImages, .continueInBackground], completed: nil)
    }
    
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
    
}
